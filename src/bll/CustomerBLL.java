package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import bll.validators.CodPostalValidator;
import bll.validators.CustomerTelefonValidator;
import bll.validators.EmailValidator;
import bll.validators.Validator;
import dao.CustomerDAO;
import model.Address;
import model.Customer;

public class CustomerBLL {
	private List<Validator<Customer>> validators;
	private Validator<Address> addrValidator;
	public CustomerBLL(){
		validators = new ArrayList<Validator<Customer>>();
		validators.add(new EmailValidator());
		validators.add(new CustomerTelefonValidator());
		addrValidator = new CodPostalValidator();
	}
	
	public Customer findCustomerById(int customerId){
		Customer customer = CustomerDAO.findById(customerId);
		if (customer == null)
			throw new NoSuchElementException("The customer with id "+customerId+" was not found!");
		return customer;
	}
	
	public Customer findCustomerByName(String customerName){
		Customer customer = CustomerDAO.findByName(customerName);
		if (customer == null)
			throw new NoSuchElementException("The customer with name "+customerName+" was not found!");
		return customer;
	}
	public Address findAddressByName(String customerName){
		Address address = CustomerDAO.findAddressByName(customerName);
		if (address == null)
			throw new NoSuchElementException("The customer with name "+customerName+" was not found!");
		return address;
	}
	public int insertCustomer(Customer customer, Address address){
		for (Validator<Customer> v : validators)
			v.validate(customer);
		addrValidator.validate(address);
		return CustomerDAO.insert(customer, address);
	}
	public boolean updateCustomer(Customer customer, Address address, String cust){
		for (Validator<Customer> v : validators)
			v.validate(customer);
		addrValidator.validate(address);
		boolean isValid = CustomerDAO.update(customer, address, cust);
		if (!isValid)
			throw new NoSuchElementException("The customer with name "+cust+" was not found!!!");
		return isValid;
	}
	public void deleteCustomer(String nume){
		CustomerDAO.delete(nume);
	}
	public List<Customer> getAllCustomers(){
		List<Customer> customers = CustomerDAO.getAllCustomers();
		return customers;
	}
	public List<Address> getAllAddresses(){
		List<Address> addresses = CustomerDAO.getAllAddresses();
		return addresses;
	}
}
