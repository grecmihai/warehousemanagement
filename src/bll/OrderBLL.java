package bll;

import java.util.NoSuchElementException;

import bll.validators.OrderCantitateValidator;
import bll.validators.Validator;
import dao.OrderDAO;
import model.Order;
import model.Struct;

public class OrderBLL {
	Validator<Order> orderValidator;
	
	public OrderBLL(){
		orderValidator = new OrderCantitateValidator();
	}
	public Order findOrderById(int orderId){
		Order order = OrderDAO.findById(orderId);
		if (order == null)
			throw new NoSuchElementException("The order with id "+orderId+" was not found!");
		return order;
	}
	public Struct insertOrder(Order order){
		orderValidator.validate(order);
		return OrderDAO.insert(order);
	}
}
