package bll;

import java.util.List;
import java.util.NoSuchElementException;

import dao.ProductDAO;
import model.Product;
import model.Stock;

public class ProductBLL {
	
	public Product findProductById(int productId){
		Product product = ProductDAO.findById(productId);
		if (product == null)
			throw new NoSuchElementException("The product with id "+productId+" was not found!");
		return product;
	}
	
	public Product findProductByName(String productName){
		Product product = ProductDAO.findByName(productName);
		if (product == null)
			throw new NoSuchElementException("The product with name "+productName+" was not found!");
		return product;
	}
	public Stock findStockByName(String productName){
		Stock stock = ProductDAO.findStockByName(productName);
		if (stock == null)
			throw new NoSuchElementException("The product with name "+productName+" was not found!");
		return stock;
	}
	public Stock findStockById(int productId){
		Stock stock = ProductDAO.findStockById(productId);
		if (stock == null)
			throw new NoSuchElementException("The product with id "+productId+" does not exist or is out of stock!");
		return stock;
	}
	public int insertProduct(Product product, Stock stock){
		return ProductDAO.insert(product, stock);
	}
	public void updateProduct(Product prod, String name){
		ProductDAO.update(prod, name);
	}
	public void increaseProductQuantity(String product,int quantity){
		ProductDAO.increaseQuantity(product, quantity);
	}
	public void decreaseProductQuantity(String product, int quantity){
		ProductDAO.decreaseQuantity(product, quantity);
	}
	public void deleteProduct(String nume){
		ProductDAO.delete(nume);
	}
	public List<Product> getAllProducts(){
		List<Product> products = ProductDAO.getAllProducts();
		return products;
	}
	public List<Stock> getAllStocks(){
		List<Stock> stocks = ProductDAO.getAllStocks();
		return stocks;
	}
}
