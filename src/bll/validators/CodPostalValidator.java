package bll.validators;

import javax.swing.JOptionPane;

import com.mysql.jdbc.StringUtils;

import model.Address;

public class CodPostalValidator implements Validator<Address>{
	
	public void validate(Address t){
		if (t.getCodPostal().length() != 6){
			JOptionPane.showMessageDialog(null, "Invalid Postal Code" , "",JOptionPane.PLAIN_MESSAGE);
			throw new IllegalArgumentException("Invalid postal code");
			
		}

		if (!StringUtils.isStrictlyNumeric(t.getCodPostal())){
			JOptionPane.showMessageDialog(null, "Fill all the informations" , "",JOptionPane.PLAIN_MESSAGE);
			throw new IllegalArgumentException("Postal code not numeric");
		}
		

	}
}
