package bll.validators;

import javax.swing.JOptionPane;

import model.Order;

public class OrderCantitateValidator implements Validator<Order>{
	
	public void validate(Order t){
		if (t.getCantitate() < 0){
			JOptionPane.showMessageDialog(null, "Invalid quantity" , "",JOptionPane.PLAIN_MESSAGE);
			throw new IllegalArgumentException("Invalid quantity"); 
		}
	}
}
