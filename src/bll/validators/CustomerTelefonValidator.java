package bll.validators;

import javax.swing.JOptionPane;

import com.mysql.jdbc.StringUtils;

import model.Customer;

public class CustomerTelefonValidator implements Validator<Customer>{
	
	public void validate(Customer t){
		char one = t.getTelefon().charAt(0);
		char two = t.getTelefon().charAt(1);
		if (one != '0' || two != '7' || t.getTelefon().length() != 10){
			JOptionPane.showMessageDialog(null, "Invalid phone number" , "",JOptionPane.PLAIN_MESSAGE);
			throw new IllegalArgumentException("Invalid phone number");
		}
		if (!StringUtils.isStrictlyNumeric(t.getTelefon())){
			JOptionPane.showMessageDialog(null, "Invalid phone number" , "",JOptionPane.PLAIN_MESSAGE);
			throw new IllegalArgumentException("Phone number not numeric");
		}
	}
}
