package model;

public class Struct {
	private boolean bol;
	private int id;
	
	public Struct(boolean bol,int id){
		this.bol = bol;
		this.id = id;
	}

	public boolean isBol() {
		return bol;
	}

	public void setBol(boolean bol) {
		this.bol = bol;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
