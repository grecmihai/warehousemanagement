package model;

public class Address {
	private int idCustomer;
	private String judet, oras, strada, codPostal;
	
	public Address(String judet, String oras, String strada, String codPostal){
		this.judet = judet;
		this.oras = oras;
		this.strada = strada;
		this.codPostal = codPostal;
	}
	public Address(int idCustomer, String judet, String oras, String strada, String codPostal){
		this.idCustomer = idCustomer;
		this.judet = judet;
		this.oras = oras;
		this.strada = strada;
		this.codPostal = codPostal;
	}
	public int getIdCustomer() {
		return idCustomer;
	}
	public void setIdCustomer(int idCustomer) {
		this.idCustomer = idCustomer;
	}
	public String getJudet() {
		return judet;
	}
	public void setJudet(String judet) {
		this.judet = judet;
	}
	public String getOras() {
		return oras;
	}
	public void setOras(String oras) {
		this.oras = oras;
	}
	public String getStrada() {
		return strada;
	}
	public void setStrada(String strada) {
		this.strada = strada;
	}
	public String getCodPostal() {
		return codPostal;
	}
	public void setCodPostal(String codPostal) {
		this.codPostal = codPostal;
	}
	@Override
	public String toString(){
		return "Address [id Customer=" + idCustomer + ", judet=" + judet + ", oras=" + oras + ", strada=" + strada + ", cod postal=" + codPostal + "]";
	}
	
	
}
