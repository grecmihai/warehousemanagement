package model;

import java.util.Date;

public class Customer {
	private int id;
	private String email,telefon,nume;
	private Date dataCreare, ultimaModificare;
	
	public Customer(String nume, String email,String telefon){
		this.email = email;
		this.nume = nume;
		this.telefon = telefon;
	}
	public Customer(int id,String nume, String email,String telefon){
		this.id = id;
		this.email = email;
		this.nume = nume;
		this.telefon = telefon;
	}
	public Customer(String nume, String email, String telefon, Date dataCreare, Date ultimaModificare){
		this.nume = nume;
		this.email = email;
		this.telefon = telefon;
		this.dataCreare = dataCreare;
		this.ultimaModificare = ultimaModificare;
	}
	public Customer(int id, String nume, String email, String telefon, Date dataCreare, Date ultimaModificare){
		this.id = id;
		this.nume = nume;
		this.email = email;
		this.telefon = telefon;
		this.dataCreare = dataCreare;
		this.ultimaModificare = ultimaModificare;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public Date getDataCreare() {
		return dataCreare;
	}

	public void setDataCreare(Date dataCreare) {
		this.dataCreare = dataCreare;
	}

	public Date getUltimaModificare() {
		return ultimaModificare;
	}

	public void setUltimaModificare(Date ultimaModificare) {
		this.ultimaModificare = ultimaModificare;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}
	@Override
	public String toString(){
		return "Customer [id=" + id + ", nume=" + nume + ", email=" + email + ", telefon=" + telefon + ", data crearii=" + dataCreare + ", ultima modificare=" + ultimaModificare + "]";
	}
	
	
}	
