package model;

public class Stock {
	private int idProduct,cantitate;
	
	public Stock(int idProduct, int cantitate){
		this.idProduct = idProduct;
		this.cantitate = cantitate;
	}
	public Stock(int cantitate){
		this.cantitate = cantitate;
	}
	public int getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(int idProduct) {
		this.idProduct = idProduct;
	}

	public int getCantitate() {
		return cantitate;
	}

	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}
	@Override
	public String toString(){
		return "Stock [id Produs=" + idProduct + ", cantitate=" + cantitate +  "]";
	}
	
}
