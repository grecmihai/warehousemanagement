package model;

import java.util.Date;

public class Order {
	private int id, customerId,productId,cantitate;
	private Date data;
	
	public Order(int id,int customerId, int productId, int cantitate, Date data){
		this.id = id;
		this.customerId = customerId;
		this.productId = productId;
		this.cantitate = cantitate;
		this.data = data;
	}
	public Order (int customerId, int productId, int cantitate, Date data){
		this.customerId = customerId;
		this.productId = productId;
		this.cantitate = cantitate;
		this.data = data;
	}
	public Order (int customerId, int productId, int cantitate){
		this.customerId = customerId;
		this.productId = productId;
		this.cantitate = cantitate;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
	
	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getCantitate() {
		return cantitate;
	}

	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}

	@Override
	public String toString(){
		return "Order [id=" + id + ", customerId=" + customerId + ", data=" + data + ", productId="+productId+", cantitate="+cantitate+   "]";
	}
	
}
