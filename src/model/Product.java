package model;

public class Product {
	private int id;
	private String nume, descriere;
	private double pret;
	
	public Product(int id, String nume, String descriere, double pret){
		this.id = id;
		this.nume = nume;
		this.descriere = descriere;
		this.pret = pret;
	}
	public Product(int id, String nume, double pret){
		this.id = id;
		this.nume = nume;
		this.pret = pret;
		descriere = "-";
	}
	public Product(String nume,double pret){
		this.nume = nume;
		this.pret = pret;
		descriere = "-";
	}
	public Product(String nume, String descriere, double pret){
		this.nume = nume;
		this.descriere = descriere;
		this.pret = pret;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	public String getDescriere() {
		return descriere;
	}
	public void setDescriere(String descriere) {
		this.descriere = descriere;
	}
	public double getPret() {
		return pret;
	}
	public void setPret(double pret) {
		this.pret = pret;
	}
	@Override
	public String toString(){
		return "Product [id=" + id + ", nume=" + nume + ", descriere=" + descriere + ", pret=" + pret + "]";
	}
	
}
