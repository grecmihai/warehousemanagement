package start;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import com.mysql.jdbc.StringUtils;

import dao.CustomerDAO;
import dao.OrderDAO;
import dao.ProductDAO;
import model.Address;
import model.Customer;
import model.Order;
import model.Product;
import model.Stock;
import presentation.controller.WarehouseController;
import presentation.view.CustomerPage;
import presentation.view.MainPage;
import presentation.view.ProductPage;
import presentation.view.WarehouseView;

public class Start {
	protected static final Logger LOGGER = Logger.getLogger(Start.class.getName());
	public static void main(String[] args) {
		//Address address = CustomerDAO.findAddressByName("ana");
		//System.out.println(address);
		//Customer customer = CustomerDAO.findById(1001);
		//System.out.print(customer);
		//Customer customer = new Customer("DIVERTUS","divertus@gmail.com","0754392142");
		//Address address = new Address("Salaj","Simleu","Florilor","450012");
		//int id = CustomerDAO.insert(customer,address);
		//System.out.println(id);
		//Customer customer = CustomerDAO.findByName("DIVERTA");
		//System.out.println(customer);
		//Customer customer = new Customer("CEMACON","contact@cemacom.com","0764219214");
		//Address address = new Address("Cluj","Cluj-Napoca","Narciselor","142996");
		//CustomerDAO.update(customer, address, "CEMACOM");
		//CustomerDAO.delete("DIVERTUS");
		//List<Customer> customers = CustomerDAO.getAllCustomers();
		//for (Customer customer : customers)
		//	System.out.println(customer);
		
		//Product product = ProductDAO.findByName("PIX BIC A");
		//System.out.println(product);
		//Product product = new Product("vraja","ie vrageala",2.4);
		//Stock stock = new Stock(500);
		//int id = ProductDAO.insert(product, stock);
		//System.out.println(id);
		//Product product = new Product("adevarat","nu mai ie vrageala",3.5);
		//ProductDAO.update(product, "vraja");
		//ProductDAO.decreaseQuantity("adevarat", 100);
		//ProductDAO.delete("adevarat");
		//List<Stock> products = ProductDAO.getAllStocks();
		//for (Stock address : products)
		//	System.out.println(address);
		
		//Stock stock = ProductDAO.findStockById(10001);
		//System.out.println(stock);
		//Order order = new Order(1005,10002,170);
		//if (!OrderDAO.insert(order))
		//	System.out.println("Insuficiente resurse");
		
		//MainPage main = new MainPage();
		//main.setVisible(true);
		
		WarehouseView warehouseView = WarehouseView.getInstance();
		WarehouseController warehouseController = new WarehouseController(warehouseView);
		warehouseView.getMainPage().setVisible(true);
		
		

		
	}
	

}
