package presentation.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class ViewProductPage extends JFrame{
	
	private static final long serialVersionUID = 1L;
	private JPanel panel;
	private JTable table,table2;
	private JScrollPane scrollPane,scrollPane2;
	
	public ViewProductPage(){
		super("Product Catalog");
		this.setLocation(500, 200);
		this.setSize(900, 550);
		panel = new JPanel();
		this.setContentPane(panel);
		panel.setLayout(new GridLayout(0,2));
	}
	public void setTables(JTable table,JTable table2) {
		this.table = table;
		this.table2 = table2;
		scrollPane = new JScrollPane(table);
		scrollPane2 = new JScrollPane(table2);
		panel.removeAll();
		panel.add(scrollPane);
		panel.add(scrollPane2);
		panel.revalidate();
	}
}
