package presentation.view;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class ProductPage extends JFrame{
	private static final long serialVersionUID = 1L;
	private JPanel panel;
	private JButton addButton,editButton,deleteButton,viewButton,backButton;
	
	public ProductPage(){
		super("Product");
		panel = new JPanel();
		this.setContentPane(panel);
		this.setSize(600, 540);
		this.setLocation(600, 280);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		addButton = new JButton("ADD");
		editButton = new JButton("EDIT");
		deleteButton = new JButton("DELETE");
		viewButton = new JButton("VIEW ALL");
		backButton = new JButton("BACK");
		
		addButton.setFont(new Font(addButton.getFont().getName() , Font.BOLD  , addButton.getFont().getSize() + 5));
		editButton.setFont(new Font(editButton.getFont().getName() , Font.BOLD  , editButton.getFont().getSize() + 5));
		deleteButton.setFont(new Font(deleteButton.getFont().getName() , Font.BOLD  , deleteButton.getFont().getSize() + 5));
		viewButton.setFont(new Font(viewButton.getFont().getName() , Font.BOLD  , viewButton.getFont().getSize() + 5));
		
		panel.setLayout(null);
		
		addButton.setBounds(200,50,150,50);
		editButton.setBounds(200,150,150,50);
		deleteButton.setBounds(200,250,150,50);
		viewButton.setBounds(200,350,150,50);
		backButton.setBounds(15,15,120,30);
		
		panel.add(addButton);
		panel.add(editButton);
		panel.add(deleteButton);
		panel.add(viewButton);
		panel.add(backButton);
	}
	public void addAddButtonListener(ActionListener listener){
		addButton.addActionListener(listener);
	}
	public void addEditButtonListener(ActionListener listener){
		editButton.addActionListener(listener);
	}
	public void addDeleteButtonListener(ActionListener listener){
		deleteButton.addActionListener(listener);
	}
	public void addViewButtonListener(ActionListener listener){
		viewButton.addActionListener(listener);
	}
	public void addBackButtonListener(ActionListener listener){
		backButton.addActionListener(listener);
	}
}
