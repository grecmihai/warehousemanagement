package presentation.view;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class AddOrderPage extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JLabel customerL,productL;
	private JComboBox customerCB,productCB;
	private JTextField quantity;
	private JButton executeButton,backButton;
	private JPanel panel;
	
	public AddOrderPage(){
		super("Add order");
		this.setLocation(500, 175);
		this.setSize(800, 500);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		panel = new JPanel();
		this.setContentPane(panel);
		panel.setLayout(null);
		
		customerL = new JLabel("Customer:");
		productL = new JLabel("Product:");
		quantity = new JTextField(10);
		executeButton = new JButton("ADAUGA");
		backButton = new JButton("BACK");
		
		
		customerL.setFont(new Font(customerL.getFont().getName(),Font.BOLD,customerL.getFont().getSize() + 10));
		productL.setFont(customerL.getFont());
		
		backButton.setBounds(15,15,120,30);
		customerL.setBounds(50, 120, 200, 40);
		productL.setBounds(350, 125, 120, 30);
		quantity.setBounds(600,220,100,40);
		executeButton.setBounds(350,350,100,40);
		
		panel.add(backButton);
		panel.add(customerL);
		panel.add(productL);
		panel.add(quantity);
		panel.add(executeButton);
	}
	public void addExecuteButtonListener(ActionListener listener){
		executeButton.addActionListener(listener);
	}
	public void addBackButtonListener(ActionListener listener){
		backButton.addActionListener(listener);
	}
	public void setComboBoxes(JComboBox comboBox1, JComboBox comboBox2){
		if (customerCB != null)
			panel.remove(customerCB);
		if (productCB != null)
			panel.remove(productCB);
		customerCB = comboBox1;
		productCB = comboBox2;
		customerCB.setBounds(50, 220, 200, 40);
		productCB.setBounds(350, 220, 200, 40);
		panel.add(customerCB);
		panel.add(productCB);
		panel.revalidate();
	}
	public JComboBox getCustomerCB() {
		return customerCB;
	}
	public void setCustomerCB(JComboBox customerCB) {
		this.customerCB = customerCB;
	}
	public JComboBox getProductCB() {
		return productCB;
	}
	public void setProductCB(JComboBox productCB) {
		this.productCB = productCB;
	}
	public JTextField getQuantity() {
		return quantity;
	}
	

}
