package presentation.view;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class AddCustomerPage extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JLabel numeL,emailL,telefonL,judetL,orasL,stradaL,codPostalL;
	private JTextField numeT,emailT,telefonT,judetT,orasT,stradaT,codPostalT;
	private JPanel panel;
	private JButton executeButton,backButton;
	
	public AddCustomerPage(){
		super("Add customer");
		this.setLocation(500, 200);
		this.setSize(700, 750);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		panel = new JPanel();
		this.setContentPane(panel);
		panel.setLayout(null);
		
		numeL = new JLabel("Nume:");
		emailL = new JLabel("e-mail:");
		telefonL = new JLabel("Telefon:");
		judetL = new JLabel("Judet:");
		orasL = new JLabel("Oras:");
		stradaL = new JLabel("Strada:");
		codPostalL = new JLabel("Cod postal:");
		numeT = new JTextField(30);
		emailT = new JTextField(30);
		telefonT = new JTextField(30);
		judetT = new JTextField(30);
		orasT = new JTextField(30);
		stradaT = new JTextField(30);
		codPostalT = new JTextField(30);
		executeButton = new JButton("ADAUGA");
		backButton = new JButton("BACK");
		
		numeL.setFont(new Font(numeL.getFont().getName(),Font.BOLD,numeL.getFont().getSize() + 5));
		emailL.setFont(numeL.getFont());
		telefonL.setFont(numeL.getFont());
		judetL.setFont(numeL.getFont());
		orasL.setFont(numeL.getFont());
		stradaL.setFont(numeL.getFont());
		codPostalL.setFont(numeL.getFont());
		numeT.setFont(new Font(numeT.getFont().getName(),Font.PLAIN,numeT.getFont().getSize() + 5));
		emailT.setFont(numeT.getFont());
		telefonT.setFont(numeT.getFont());
		judetT.setFont(numeT.getFont());
		orasT.setFont(numeT.getFont());
		stradaT.setFont(numeT.getFont());
		codPostalT.setFont(numeT.getFont());
		
		numeL.setBounds(150,80,150,20);
		emailL.setBounds(150,150,150,20);
		telefonL.setBounds(150,220,150,20);
		judetL.setBounds(150,290,150,20);
		orasL.setBounds(150,360,150,20);
		stradaL.setBounds(150,430,150,20);
		codPostalL.setBounds(150,500,150,20);
		
		numeT.setBounds(250,80,300,40);
		emailT.setBounds(250,150,300,40);
		telefonT.setBounds(250,220,300,40);
		judetT.setBounds(250,290,300,40);
		orasT.setBounds(250,360,300,40);
		stradaT.setBounds(250,430,300,40);
		codPostalT.setBounds(250,500,300,40);
		
		executeButton.setBounds(250,600,150,40);
		backButton.setBounds(15,15,120,30);
		
		panel.add(numeL);
		panel.add(emailL);
		panel.add(telefonL);
		panel.add(judetL);
		panel.add(orasL);
		panel.add(stradaL);
		panel.add(codPostalL);
		panel.add(numeT);
		panel.add(emailT);
		panel.add(telefonT);
		panel.add(judetT);
		panel.add(orasT);
		panel.add(stradaT);
		panel.add(codPostalT);
		panel.add(executeButton);
		panel.add(backButton);
		
	}

	public JTextField getNumeT() {
		return numeT;
	}

	public void setNumeT(JTextField numeT) {
		this.numeT = numeT;
	}

	public JTextField getEmailT() {
		return emailT;
	}

	public void setEmailT(JTextField emailT) {
		this.emailT = emailT;
	}

	public JTextField getTelefonT() {
		return telefonT;
	}

	public void setTelefonT(JTextField telefonT) {
		this.telefonT = telefonT;
	}

	public JTextField getJudetT() {
		return judetT;
	}

	public void setJudetT(JTextField judetT) {
		this.judetT = judetT;
	}

	public JTextField getOrasT() {
		return orasT;
	}

	public void setOrasT(JTextField orasT) {
		this.orasT = orasT;
	}

	public JTextField getStradaT() {
		return stradaT;
	}

	public void setStradaT(JTextField stradaT) {
		this.stradaT = stradaT;
	}

	public JTextField getCodPostalT() {
		return codPostalT;
	}

	public void setCodPostalT(JTextField codPostalT) {
		this.codPostalT = codPostalT;
	}
	public void addExecuteButtonListener(ActionListener listener){
		executeButton.addActionListener(listener);
	}
	public void addBackButtonListener(ActionListener listener){
		backButton.addActionListener(listener);
	}
	
}
