package presentation.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class MainPage extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel mainPanel;
	private JButton customerButton, productButton, orderButton;
	
	public MainPage(){
		
		super("Main");
		mainPanel = new JPanel();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setContentPane(mainPanel);
		this.setSize(600, 540);
		this.setLocation(600, 280);
		
		customerButton = new JButton("CUSTOMER");
		productButton = new JButton("PRODUCT");
		orderButton = new JButton("ORDER");
		
		customerButton.setBackground(new Color(214,137,33));
		productButton.setBackground(new Color(214,137,33));
		orderButton.setBackground(new Color(214,137,33));
		
		customerButton.setFont(new Font(customerButton.getFont().getName() , Font.BOLD  , customerButton.getFont().getSize() + 20));
		productButton.setFont(new Font(customerButton.getFont().getName() , Font.BOLD  , productButton.getFont().getSize() + 20));
		orderButton.setFont(new Font(customerButton.getFont().getName() , Font.BOLD  , orderButton.getFont().getSize() + 20));

		mainPanel.setLayout(new GridLayout(3,0));
		mainPanel.add(customerButton);
		mainPanel.add(productButton);
		mainPanel.add(orderButton);
	}
	
	public void addCustomerButtonListener(ActionListener listener){
		customerButton.addActionListener(listener);
	}
	public void addProductButtonListener(ActionListener listener){
		productButton.addActionListener(listener);
	}
	public void addOrderButtonListener(ActionListener listener){
		orderButton.addActionListener(listener);
	}
}
