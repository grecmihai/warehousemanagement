package presentation.view;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class AddProductPage extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel numeL,descriereL,pretL,cantitateL;
	private JTextField numeT,descriereT,pretT,cantitateT;
	private JPanel panel;
	private JButton executeButton;
	private JButton backButton;
	
	public AddProductPage(){
		super("Add product");
		this.setLocation(500, 200);
		this.setSize(700, 550);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		panel = new JPanel();
		this.setContentPane(panel);
		panel.setLayout(null);
		
		numeL = new JLabel("Nume*:");
		descriereL = new JLabel("Descriere:");
		pretL = new JLabel("Pret*:");
		cantitateL = new JLabel("Cantitate*:");
		numeT = new JTextField(30);
		descriereT = new JTextField("-",30);
		pretT = new JTextField(30);
		cantitateT = new JTextField(30);
		executeButton = new JButton("ADAUGA");
		backButton = new JButton("BACK");
		
		numeL.setFont(new Font(numeL.getFont().getName(),Font.BOLD,numeL.getFont().getSize() + 5));
		descriereL.setFont(numeL.getFont());
		pretL.setFont(numeL.getFont());
		cantitateL.setFont(numeL.getFont());
		numeT.setFont(new Font(numeT.getFont().getName(),Font.PLAIN,numeT.getFont().getSize() + 5));
		descriereT.setFont(numeT.getFont());
		pretT.setFont(numeT.getFont());
		cantitateT.setFont(numeT.getFont());
		
		numeL.setBounds(150,80,150,20);
		descriereL.setBounds(150,150,150,20);
		pretL.setBounds(150,220,150,20);
		cantitateL.setBounds(150,290,150,20);
		
		numeT.setBounds(250,80,300,40);
		descriereT.setBounds(250,150,300,40);
		pretT.setBounds(250,220,300,40);
		cantitateT.setBounds(250,290,300,40);
		
		executeButton.setBounds(250,390,150,40);
		backButton.setBounds(15,15,120,30);
		
		panel.add(numeL);
		panel.add(descriereL);
		panel.add(pretL);
		panel.add(cantitateL);
		panel.add(numeT);
		panel.add(descriereT);
		panel.add(pretT);
		panel.add(cantitateT);
		panel.add(executeButton);
		panel.add(backButton);
	}

	public JTextField getNumeT() {
		return numeT;
	}

	public void setNumeT(JTextField numeT) {
		this.numeT = numeT;
	}

	public JTextField getDescriereT() {
		return descriereT;
	}

	public void setDescriereT(JTextField descriereT) {
		this.descriereT = descriereT;
	}

	public JTextField getPretT() {
		return pretT;
	}

	public void setPretT(JTextField pretT) {
		this.pretT = pretT;
	}

	public JTextField getCantitateT() {
		return cantitateT;
	}

	public void setCantitateT(JTextField cantitateT) {
		this.cantitateT = cantitateT;
	}
	
	public void addExecuteButtonListener(ActionListener listener){
		executeButton.addActionListener(listener);
	}
	public void addBackButtonListener(ActionListener listener){
		backButton.addActionListener(listener);
	}
	
}
