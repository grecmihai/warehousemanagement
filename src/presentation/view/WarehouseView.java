package presentation.view;

public class WarehouseView {
	private static WarehouseView instance = null;
	private CustomerPage customerPage;
	private ProductPage productPage;
	private MainPage mainPage;
	private AddCustomerPage addCustomerPage;
	private AddProductPage addProductPage;
	private EditProductPage editProductPage;
	private ViewCustomerPage viewCustomerPage;
	private ViewProductPage viewProductPage;
	private EditCustomerPage editCustomerPage;
	private DeleteCustomerPage deleteCustomerPage;
	private DeleteProductPage deleteProductPage;
	private AddOrderPage addOrderPage;
	
	private WarehouseView(){
		customerPage = new CustomerPage();
		productPage = new ProductPage();
		mainPage = new MainPage();
		addCustomerPage = new AddCustomerPage();
		addProductPage = new AddProductPage();
		editProductPage = new EditProductPage();
		viewCustomerPage = new ViewCustomerPage();
		viewProductPage = new ViewProductPage();
		editCustomerPage = new EditCustomerPage();
		deleteCustomerPage = new DeleteCustomerPage();
		deleteProductPage = new DeleteProductPage();
		addOrderPage = new AddOrderPage();
	}
	
	public static WarehouseView getInstance(){
		if (instance == null)
			instance = new WarehouseView();
		return instance;
	}

	public CustomerPage getCustomerPage() {
		return customerPage;
	}

	public ProductPage getProductPage() {
		return productPage;
	}

	public MainPage getMainPage() {
		return mainPage;
	}

	public AddCustomerPage getAddCustomerPage() {
		return addCustomerPage;
	}

	public AddProductPage getAddProductPage() {
		return addProductPage;
	}

	public EditProductPage getEditProductPage() {
		return editProductPage;
	}

	public ViewCustomerPage getViewCustomerPage() {
		return viewCustomerPage;
	}

	public ViewProductPage getViewProductPage() {
		return viewProductPage;
	}

	public EditCustomerPage getEditCustomerPage() {
		return editCustomerPage;
	}

	public DeleteCustomerPage getDeleteCustomerPage() {
		return deleteCustomerPage;
	}

	public DeleteProductPage getDeleteProductPage() {
		return deleteProductPage;
	}

	public AddOrderPage getAddOrderPage() {
		return addOrderPage;
	}
	
	
	
	
}
