package presentation.view;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class DeleteCustomerPage extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel panel;
	private JComboBox comboBox;
	private JButton executeButton,backButton;
	
	public DeleteCustomerPage(){
		super("Delete customer");
		this.setLocation(500, 175);
		this.setSize(600, 300);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		panel = new JPanel();
		this.setContentPane(panel);
		panel.setLayout(null);
		
		executeButton = new JButton("STERGE");
		backButton = new JButton("BACK");
		
		backButton.setBounds(15,15,120,30);
		executeButton.setBounds(400,125,120,30);
		
		panel.add(backButton);
		panel.add(executeButton);

	}
	public void addExecuteButtonListener(ActionListener listener){
		executeButton.addActionListener(listener);
	}
	public void addBackButtonListener(ActionListener listener){
		backButton.addActionListener(listener);
	}
	public void setComboBox(JComboBox comboBox){
		if (this.comboBox != null)
			panel.remove(this.comboBox);
		this.comboBox = comboBox;
		this.comboBox.setBounds(50,120,200,40);
		panel.add(this.comboBox);
		panel.revalidate();
	}
	public JComboBox getComboBox() {
		return comboBox;
	}
	
}
