package presentation.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;

import bll.CustomerBLL;
import bll.OrderBLL;
import bll.ProductBLL;
import model.Address;
import model.Customer;
import model.Order;
import model.Product;
import model.Stock;
import model.Struct;
import presentation.view.WarehouseView;
import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;

public class WarehouseController {
	private WarehouseView warehouseView;
	private CustomerBLL customerBLL;
	private ProductBLL productBLL;
	private OrderBLL orderBLL;
	
	public WarehouseController(WarehouseView view){
		warehouseView = view;
		customerBLL = new CustomerBLL();
		productBLL = new ProductBLL();
		orderBLL = new OrderBLL();
		warehouseView.getMainPage().addCustomerButtonListener(new CustomerButtonListener());
		warehouseView.getMainPage().addProductButtonListener(new ProductButtonListener());
		warehouseView.getCustomerPage().addAddButtonListener(new CustomerAddButtonListener());
		warehouseView.getCustomerPage().addBackButtonListener(new CustomerBackButtonListener());
		warehouseView.getCustomerPage().addEditButtonListener(new CustomerEditButtonListener());
		warehouseView.getCustomerPage().addDeleteButtonListener(new CustomerProductDeleteButtonListener());
		warehouseView.getCustomerPage().addViewButtonListener(new CustomerViewButtonListener());
		warehouseView.getProductPage().addAddButtonListener(new ProductAddButtonListener());
		warehouseView.getProductPage().addBackButtonListener(new ProductBackButtonListener());
		warehouseView.getProductPage().addEditButtonListener(new ProductEditButtonListener());
		warehouseView.getProductPage().addDeleteButtonListener(new ProductDeleteButtonListener());
		warehouseView.getProductPage().addViewButtonListener(new ProductViewButtonListener());
		warehouseView.getAddCustomerPage().addBackButtonListener(new AddCustomerBackButtonListener());
		warehouseView.getAddCustomerPage().addExecuteButtonListener(new AddCustomerExecuteButton());
		warehouseView.getAddProductPage().addBackButtonListener(new AddProductBackButtonListener());
		warehouseView.getAddProductPage().addExecuteButtonListener(new AddProductExecuteButton());
		warehouseView.getEditCustomerPage().addBackButtonListener(new EditCustomerBackButtonListener());
		warehouseView.getEditCustomerPage().addExecuteButtonListener(new EditCustomerListener());
		warehouseView.getEditProductPage().addExecuteButtonListener(new EditProductListener());
		warehouseView.getEditProductPage().addBackButtonListener(new EditProductBackButtonListener());
		warehouseView.getDeleteCustomerPage().addBackButtonListener(new DeleteCustomerBackButtonListener());
		warehouseView.getDeleteCustomerPage().addExecuteButtonListener(new DeleteCustomerListener());
		warehouseView.getDeleteProductPage().addBackButtonListener(new DeleteProductBackButtonListener());
		warehouseView.getDeleteProductPage().addExecuteButtonListener(new DeleteProductListener());
		warehouseView.getMainPage().addOrderButtonListener(new OrderButtonListener());
		warehouseView.getAddOrderPage().addBackButtonListener(new OrderBackButtonListener());
		warehouseView.getAddOrderPage().addExecuteButtonListener(new OrderListener());
	}
	private static JTable createTable(List<Object> objects) throws IllegalArgumentException, IllegalAccessException{
		String[] columnNames;
		Object[][] data;
		int noOfColumns = 0;
		for (Field field : objects.get(0).getClass().getDeclaredFields())
			noOfColumns++;
		columnNames = new String[noOfColumns];
		data = new Object[objects.size()][noOfColumns];
		int i = 0;
		for (Field field : objects.get(0).getClass().getDeclaredFields()){
			columnNames[i] = field.getName();
			i++;
		}
		i = 0;
		int j = 0;
		for (Object object : objects)
			for (Field field : object.getClass().getDeclaredFields()){
				field.setAccessible(true);
				data[i][j] = field.get(object);
				
				j++;
				if (j == noOfColumns){
					i++;
					j = 0;
				}
			}
		JTable table = new JTable(data,columnNames);
		return table;
	}
	private static JComboBox createComboBox(List<Object> objects) throws IllegalArgumentException, IllegalAccessException{
		DefaultComboBoxModel model = new DefaultComboBoxModel();
		for (Object object : objects)
			for (Field field : object.getClass().getDeclaredFields()){
				field.setAccessible(true);
				if (field.getName().equals("nume"))
					model.addElement(field.get(object));
			}
		JComboBox comboBox = new JComboBox(model);
		comboBox.setSelectedIndex(0);
		comboBox.setSelectedItem(0);
		return comboBox;
	}
	private static String createPDF(int orderNo, String customerName, Date date, int cantitate, String product, double pret){
		Document document = new Document(PageSize.A5);
		try{
			PdfWriter.getInstance(document, new FileOutputStream(String.valueOf(orderNo)+".pdf"));
			document.open();
			Paragraph p1 = new Paragraph("Order#"+String.valueOf(orderNo));
			Paragraph p2 = new Paragraph("Customer:"+customerName);
			Paragraph p3 = new Paragraph("Date:"+String.valueOf(date));
			Paragraph p4 = new Paragraph("Product:"+String.valueOf(cantitate)+"       "+product+"       "+String.valueOf(pret));
			Paragraph p5 = new Paragraph("Total price:"+String.valueOf(cantitate*pret));
			document.add(p1);
			document.add(p2);
			document.add(p3);
			document.add(p4);
			document.add(p5);
		}
		catch(Exception e){
			System.out.println(e);
		}
		finally{
			document.close();
		}
		return String.valueOf(orderNo)+".pdf";
	}
	private static void sendMail(String email,String pdf){
		String to = email;
		/*
	    String host = "localhost";
	    Properties properties = System.getProperties();
	    properties.setProperty("mail.smtp.host", host);
	  
		Session session = Session.getDefaultInstance(properties,
			new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication("mihai_amdp4@yahoo.com","steaua1996");
				}
			});
		*/
		/*
		final String username = "mihai_amdp4@yahoo.com";
		final String password = "steaua1996";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		  });
		  */
		String user = "grec_mihai96@gmail.com";
		String pass = "altair1ezioauditore234";
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "587");
		props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.user", user);
        props.put("mail.password", pass);
        
        Authenticator auth = new Authenticator() {
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(user, pass);
            }
        };

		Session session = Session.getInstance(props, auth);

		
		try{
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(user));
			InternetAddress[] toAddress = {new InternetAddress(to)};
			message.setRecipients(Message.RecipientType.TO,toAddress);
			message.setSubject("Order");
			message.setSentDate(new Date());
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent("I have attached the Order receipt","text/html");
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);
			MimeBodyPart attachPart = new MimeBodyPart();
			attachPart.attachFile(pdf);
			multipart.addBodyPart(attachPart);
			message.setContent(multipart);
			Transport.send(message);
			System.out.println("mail trimis");
			
			
		}
		catch (MessagingException mex) {
	         mex.printStackTrace();
	      } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public class CustomerButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			warehouseView.getCustomerPage().setVisible(true);
			warehouseView.getMainPage().setVisible(false);
		}
	}
	public class CustomerBackButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			warehouseView.getCustomerPage().setVisible(false);
			warehouseView.getMainPage().setVisible(true);
		}
	}
	public class ProductButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			warehouseView.getProductPage().setVisible(true);
			warehouseView.getMainPage().setVisible(false);
		}
	}
	public class ProductBackButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			warehouseView.getProductPage().setVisible(false);
			warehouseView.getMainPage().setVisible(true);
		}
	}
	public class CustomerAddButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			warehouseView.getAddCustomerPage().setVisible(true);
			warehouseView.getCustomerPage().setVisible(false);
		}
	}
	public class ProductAddButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			warehouseView.getAddProductPage().setVisible(true);
			warehouseView.getProductPage().setVisible(false);
		}
	}
	public class CustomerEditButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			
			List<Customer> customer = customerBLL.getAllCustomers();
			List<Object> objects = new ArrayList<Object>(customer);
			
			try {
				warehouseView.getEditCustomerPage().setComboBox(createComboBox(objects));
				warehouseView.getCustomerPage().setVisible(false);
				warehouseView.getEditCustomerPage().setVisible(true);
				String name = String.valueOf(warehouseView.getEditCustomerPage().getComboBox().getSelectedItem());
				Customer cust = customerBLL.findCustomerByName(name);
				Address addr = customerBLL.findAddressByName(name);
				warehouseView.getEditCustomerPage().setTexts(cust.getNume(), cust.getEmail(),
						cust.getTelefon(), addr.getJudet(), addr.getOras(), addr.getStrada(), addr.getCodPostal());
				warehouseView.getEditCustomerPage().addComboBoxListener(new CustomerComboBoxListener(),warehouseView.getEditCustomerPage().getComboBox());
			} catch (IllegalArgumentException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	public class CustomerComboBoxListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			String name = String.valueOf(warehouseView.getEditCustomerPage().getComboBox().getSelectedItem());
			Customer cust = customerBLL.findCustomerByName(name);
			Address addr = customerBLL.findAddressByName(name);
			warehouseView.getEditCustomerPage().setTexts(cust.getNume(), cust.getEmail(),
					cust.getTelefon(), addr.getJudet(), addr.getOras(), addr.getStrada(), addr.getCodPostal());
		}
	}
	public class EditCustomerBackButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			warehouseView.getCustomerPage().setVisible(true);
			warehouseView.getEditCustomerPage().setVisible(false);
		}
	}
	public class ProductEditButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			
			List<Product> products = productBLL.getAllProducts();
			List<Object> objects = new ArrayList<Object>(products);
			try {
				warehouseView.getEditProductPage().setComboBox(createComboBox(objects));
				warehouseView.getProductPage().setVisible(false);
				warehouseView.getEditProductPage().setVisible(true);
				String name = String.valueOf(warehouseView.getEditProductPage().getComboBox().getSelectedItem());
				Product product = productBLL.findProductByName(name);
				Stock stock = productBLL.findStockByName(name);
				warehouseView.getEditProductPage().setTexts(product.getNume(), product.getDescriere(), 
						String.valueOf(product.getPret()), String.valueOf(stock.getCantitate()));
				warehouseView.getEditProductPage().addComboBoxListener(new ProductComboBoxListener(),warehouseView.getEditProductPage().getComboBox());
			} catch (IllegalArgumentException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	public class ProductComboBoxListener implements ItemListener{
		public void itemStateChanged(ItemEvent event){
			if (event.getStateChange() == ItemEvent.SELECTED){
			String name = String.valueOf(warehouseView.getEditProductPage().getComboBox().getSelectedItem());
			Product product = productBLL.findProductByName(name);
			Stock stock = productBLL.findStockByName(name);
			warehouseView.getEditProductPage().setTexts(product.getNume(), product.getDescriere(), 
					String.valueOf(product.getPret()), String.valueOf(stock.getCantitate()));
			}
		}
	}
	public class EditProductBackButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			warehouseView.getProductPage().setVisible(true);
			warehouseView.getEditProductPage().setVisible(false);
		}
	}
	public class CustomerProductDeleteButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			List<Customer> customer = customerBLL.getAllCustomers();
			List<Object> objects = new ArrayList<Object>(customer);
			try {
				warehouseView.getDeleteCustomerPage().setComboBox(createComboBox(objects));
				warehouseView.getCustomerPage().setVisible(false);
				warehouseView.getDeleteCustomerPage().setVisible(true);
			} catch (IllegalArgumentException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	public class DeleteCustomerBackButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			warehouseView.getCustomerPage().setVisible(true);
			warehouseView.getDeleteCustomerPage().setVisible(false);
		}
	}
	public class ProductDeleteButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			List<Product> products = productBLL.getAllProducts();
			List<Object> objects = new ArrayList<Object>(products);
			try {
				warehouseView.getDeleteProductPage().setComboBox(createComboBox(objects));
				warehouseView.getProductPage().setVisible(false);
				warehouseView.getDeleteProductPage().setVisible(true);
			} catch (IllegalArgumentException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	public class DeleteProductBackButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			warehouseView.getProductPage().setVisible(true);
			warehouseView.getDeleteProductPage().setVisible(false);
		}
	}
	public class AddCustomerBackButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
				warehouseView.getAddCustomerPage().setVisible(false);
				warehouseView.getCustomerPage().setVisible(true);
		}
	}
	public class AddProductBackButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			warehouseView.getAddProductPage().setVisible(false);
			warehouseView.getProductPage().setVisible(true);
		}
	}
	public class OrderButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			List<Customer> customers = customerBLL.getAllCustomers();
			List<Product> products = productBLL.getAllProducts();
			List<Object> objects = new ArrayList<Object>(customers);
			List<Object> objects2 = new ArrayList<Object>(products);
			warehouseView.getAddOrderPage().getQuantity().setText("");
			try {
				warehouseView.getAddOrderPage().setComboBoxes(createComboBox(objects), createComboBox(objects2));
				warehouseView.getMainPage().setVisible(false);
				warehouseView.getAddOrderPage().setVisible(true);
			} catch (IllegalArgumentException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	public class OrderBackButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			warehouseView.getMainPage().setVisible(true);
			warehouseView.getAddOrderPage().setVisible(false);
		}
	}
	public class AddCustomerExecuteButton implements ActionListener{
		public void actionPerformed(ActionEvent event){
			if (warehouseView.getAddCustomerPage().getNumeT().getText().isEmpty() || 
					warehouseView.getAddCustomerPage().getEmailT().getText().isEmpty() ||
					warehouseView.getAddCustomerPage().getTelefonT().getText().isEmpty() ||
					warehouseView.getAddCustomerPage().getJudetT().getText().isEmpty() ||
					warehouseView.getAddCustomerPage().getOrasT().getText().isEmpty() ||
					warehouseView.getAddCustomerPage().getStradaT().getText().isEmpty() ||
					warehouseView.getAddCustomerPage().getCodPostalT().getText().isEmpty() ){
				JOptionPane.showMessageDialog(null, "Fill all the informations" , "",JOptionPane.PLAIN_MESSAGE);
			}
			else{
				Customer customer = new Customer(warehouseView.getAddCustomerPage().getNumeT().getText(),
						warehouseView.getAddCustomerPage().getEmailT().getText(),
						warehouseView.getAddCustomerPage().getTelefonT().getText());
				Address address = new Address(warehouseView.getAddCustomerPage().getJudetT().getText(),
						warehouseView.getAddCustomerPage().getOrasT().getText(),
						warehouseView.getAddCustomerPage().getStradaT().getText(),
						warehouseView.getAddCustomerPage().getCodPostalT().getText());
				customerBLL.insertCustomer(customer, address);
				JOptionPane.showMessageDialog(null, "Success!!!" , "",JOptionPane.PLAIN_MESSAGE);
			}
		}
	}
	public class AddProductExecuteButton implements ActionListener{
		public void actionPerformed(ActionEvent event){
			if (warehouseView.getAddProductPage().getNumeT().getText().isEmpty() ||
					warehouseView.getAddProductPage().getPretT().getText().isEmpty() ||
					warehouseView.getAddProductPage().getCantitateT().getText().isEmpty() ){
				JOptionPane.showMessageDialog(null, "Fill all the informations marked with *" , "",JOptionPane.PLAIN_MESSAGE);

			}
			else{
				Product product;
				if (warehouseView.getAddProductPage().getDescriereT().getText().isEmpty())
					product = new Product(warehouseView.getAddProductPage().getNumeT().getText(),Double.parseDouble(warehouseView.getAddProductPage().getPretT().getText()));
				else
					product = new Product(warehouseView.getAddProductPage().getNumeT().getText(),
							warehouseView.getAddProductPage().getDescriereT().getText(),
							Double.parseDouble(warehouseView.getAddProductPage().getPretT().getText()));
				Stock stock = new Stock(Integer.parseInt(warehouseView.getAddProductPage().getCantitateT().getText()));
				productBLL.insertProduct(product, stock);
				JOptionPane.showMessageDialog(null, "Succes!" , "",JOptionPane.PLAIN_MESSAGE);

			}
		}
	}
	public class CustomerViewButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			List<Customer> customers = customerBLL.getAllCustomers();
			List<Object> objects = new ArrayList<Object>(customers);
			List<Address> addresses = customerBLL.getAllAddresses();
			List<Object> objects2 = new ArrayList<Object>(addresses);
			try {
				warehouseView.getViewCustomerPage().setTables(createTable(objects),createTable(objects2));
				warehouseView.getViewCustomerPage().setVisible(true);
			} catch (IllegalArgumentException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	public class ProductViewButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			List<Product> products = productBLL.getAllProducts();
			List<Object> objects = new ArrayList<Object>(products);
			List<Stock> stocks = productBLL.getAllStocks();
			List<Object> objects2 = new ArrayList<Object>(stocks);
			try {
				warehouseView.getViewProductPage().setTables(createTable(objects),createTable(objects2));
				warehouseView.getViewProductPage().setVisible(true);
			} catch (IllegalArgumentException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	public class EditCustomerListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			String cust = String.valueOf(warehouseView.getEditCustomerPage().getComboBox().getSelectedItem());
			if (warehouseView.getEditCustomerPage().getNumeT().getText().isEmpty() || 
					warehouseView.getEditCustomerPage().getEmailT().getText().isEmpty() ||
					warehouseView.getEditCustomerPage().getTelefonT().getText().isEmpty() ||
					warehouseView.getEditCustomerPage().getJudetT().getText().isEmpty() ||
					warehouseView.getEditCustomerPage().getOrasT().getText().isEmpty() ||
					warehouseView.getEditCustomerPage().getStradaT().getText().isEmpty() ||
					warehouseView.getEditCustomerPage().getCodPostalT().getText().isEmpty() ){
				JOptionPane.showMessageDialog(null, "Fill all the informations" , "",JOptionPane.PLAIN_MESSAGE);
			}
			else{
				Customer customer = new Customer(warehouseView.getEditCustomerPage().getNumeT().getText(),
						warehouseView.getEditCustomerPage().getEmailT().getText(),
						warehouseView.getEditCustomerPage().getTelefonT().getText());
				Address address = new Address(warehouseView.getEditCustomerPage().getJudetT().getText(),
						warehouseView.getEditCustomerPage().getOrasT().getText(),
						warehouseView.getEditCustomerPage().getStradaT().getText(),
						warehouseView.getEditCustomerPage().getCodPostalT().getText());
				customerBLL.updateCustomer(customer, address, cust);
				JOptionPane.showMessageDialog(null, "Succes!" , "",JOptionPane.PLAIN_MESSAGE);
			}
		}
	}
	public class EditProductListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			String prod = String.valueOf(warehouseView.getEditProductPage().getComboBox().getSelectedItem());
			if (warehouseView.getEditProductPage().getNumeT().getText().isEmpty() ||
					warehouseView.getEditProductPage().getPretT().getText().isEmpty() ||
					warehouseView.getEditProductPage().getCantitateT().getText().isEmpty() ){
				JOptionPane.showMessageDialog(null, "Fill all the informations marked with *" , "",JOptionPane.PLAIN_MESSAGE);

			}
			else{
				Product product;
				if (warehouseView.getEditProductPage().getDescriereT().getText().isEmpty())
					product = new Product(warehouseView.getEditProductPage().getNumeT().getText(),Double.parseDouble(warehouseView.getAddProductPage().getPretT().getText()));
				else
					product = new Product(warehouseView.getEditProductPage().getNumeT().getText(),
							warehouseView.getEditProductPage().getDescriereT().getText(),
							Double.parseDouble(warehouseView.getEditProductPage().getPretT().getText()));
				Stock stock = new Stock(Integer.parseInt(warehouseView.getEditProductPage().getCantitateT().getText()));
				productBLL.updateProduct(product, prod);
				productBLL.increaseProductQuantity(prod, stock.getCantitate());
				JOptionPane.showMessageDialog(null, "Succes!" , "",JOptionPane.PLAIN_MESSAGE);
			}
		}
	}
	public class DeleteCustomerListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			String cust = String.valueOf(warehouseView.getDeleteCustomerPage().getComboBox().getSelectedItem());
			customerBLL.deleteCustomer(cust);
			JOptionPane.showMessageDialog(null, "Succes!" , "",JOptionPane.PLAIN_MESSAGE);
		}
	}
	public class DeleteProductListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			String prod = String.valueOf(warehouseView.getDeleteProductPage().getComboBox().getSelectedItem());
			productBLL.deleteProduct(prod);
			JOptionPane.showMessageDialog(null, "Succes!" , "",JOptionPane.PLAIN_MESSAGE);
		}
	}
	public class OrderListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			String cust = String.valueOf(warehouseView.getAddOrderPage().getCustomerCB().getSelectedItem());
			String prod = String.valueOf(warehouseView.getAddOrderPage().getProductCB().getSelectedItem());
			int quantity = Integer.parseInt(warehouseView.getAddOrderPage().getQuantity().getText());
			Customer customer = customerBLL.findCustomerByName(cust);
			Product product = productBLL.findProductByName(prod);
			Order order = new Order(customer.getId(),product.getId(),quantity);
			Struct str = orderBLL.insertOrder(order);
			boolean succes = str.isBol();
			if (succes){
				JOptionPane.showMessageDialog(null, "Succes!" , "",JOptionPane.PLAIN_MESSAGE);
				Order ordery = orderBLL.findOrderById(str.getId());
				String pdf = createPDF(ordery.getId(), cust, ordery.getData(), quantity, prod, product.getPret());
				sendMail(customer.getEmail(),pdf);
			}
			else
				JOptionPane.showMessageDialog(null, "Understock" , "",JOptionPane.PLAIN_MESSAGE);
		}
	}
	
}
