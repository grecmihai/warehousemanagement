package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.sql.Statement;

import connection.ConnectionFactory;
import model.Address;
import model.Customer;

public class CustomerDAO {
	protected static final Logger LOGGER = Logger.getLogger(CustomerDAO.class.getName());
	private static final String findStatementString = "select * from customer where id = ?";
	private static final String findAddrString = "select * from address join customer on id = idCustomer where nume = ?";
	private static final String findStatementNameString = "select * from customer where nume = ?";
	private static final String insertStatementString = "insert into customer (nume, email, telefon) values(?,?,?)";
	private static final String insertStatementAddrString = "insert into address values (?,?,?,?,?)";
	private static final String updateStatementString = "update customer set nume = ? , email = ? , telefon = ? where ID = ?";
	private static final String updateStatementAddrString = "update address set judet = ? , oras = ? , strada = ? , codPostal = ? where idCustomer = ?";
	private static final String deleteStatementString = "delete from customer where nume = ?";
	private static final String getStatementString = "select * from customer";
	private static final String getStatementAddrString = "select * from address";
	
	public static Customer findById(int customerId){
		Customer toReturn = null;
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		
		try{
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setInt(1, customerId);
			rs = findStatement.executeQuery();
			rs.next();
			
			String nume = rs.getString("nume");
			String email = rs.getString("email");
			String telefon = rs.getString("telefon");
			Date dataCreare = rs.getDate("dataCreare");
			Date ultimaModificare = rs.getDate("ultimaModificare");
			toReturn = new Customer(customerId, nume,email,telefon,dataCreare,ultimaModificare);
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING,"CustomerDAO:findById " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	
	public static Customer findByName(String name){
		Customer toReturn = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		
		try{
			findStatement = dbConnection.prepareStatement(findStatementNameString);
			findStatement.setString(1, name);
			rs = findStatement.executeQuery();
			rs.next();
			int id = rs.getInt("id");
			String email = rs.getString("email");
			String telefon = rs.getString("telefon");
			Date dataCreare = rs.getDate("dataCreare");
			Date ultimaModificare = rs.getDate("ultimaModificare");
			toReturn = new Customer(id, name,email,telefon,dataCreare,ultimaModificare);
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING,"CustomerDAO:findByName " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	public static Address findAddressByName(String name){
		Address toReturn = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try{
			findStatement = dbConnection.prepareStatement(findAddrString);
			findStatement.setString(1, name);
			rs = findStatement.executeQuery();
			rs.next();
			int id = rs.getInt("idCustomer");
			String judet = rs.getString("judet");
			String oras = rs.getString("oras");
			String strada = rs.getString("strada");
			String codPostal = rs.getString("codPostal");
			toReturn = new Address(id,judet,oras,strada,codPostal);
			
			
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING,"CustomerDAO:findByName " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	public static int insert(Customer customer,Address address){
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement insertStatement= null;
		PreparedStatement insertStatementAddr = null;
		int insertedId = -1;
		
		try{
			insertStatement = dbConnection.prepareStatement(insertStatementString,Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, customer.getNume());
			insertStatement.setString(2, customer.getEmail());
			insertStatement.setString(3, customer.getTelefon());
			insertStatement.executeUpdate();
			
			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next())
			insertedId = rs.getInt(1);
			
			insertStatementAddr = dbConnection.prepareStatement(insertStatementAddrString);
			insertStatementAddr.setInt(1, insertedId);
			insertStatementAddr.setString(2, address.getJudet());
			insertStatementAddr.setString(3, address.getOras());
			insertStatementAddr.setString(4, address.getStrada());
			insertStatementAddr.setString(5, address.getCodPostal());
			insertStatementAddr.executeUpdate();
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "CustomerDAO:insert " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(dbConnection);
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(insertStatementAddr);
		}
		return insertedId;
	}
	
	public static boolean update(Customer customer , Address address, String cust){
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement= null;
		PreparedStatement updateStatementAddr = null;
		int id = -1;
		boolean validCustomer = true;
		try{
			Customer helper = findByName(cust);
			id = helper.getId();
			
			if (id != -1){
				updateStatement = dbConnection.prepareStatement(updateStatementString);
				updateStatement.setString(1, customer.getNume());
				updateStatement.setString(2, customer.getEmail());
				updateStatement.setString(3, customer.getTelefon());
				updateStatement.setInt(4, id);
				updateStatement.executeUpdate();
			
				updateStatementAddr = dbConnection.prepareStatement(updateStatementAddrString);
				updateStatementAddr.setString(1, address.getJudet());
				updateStatementAddr.setString(2, address.getOras());
				updateStatementAddr.setString(3, address.getStrada());
				updateStatementAddr.setString(4, address.getCodPostal());
				updateStatementAddr.setInt(5, id);
				updateStatementAddr.executeUpdate();
			}
			else {
				validCustomer = false;
			}
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "CustomerDAO:update " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(dbConnection);
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(updateStatementAddr);
		}
		return validCustomer;
	}
	
	public static void delete(String nume){
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString);
			deleteStatement.setString(1, nume);
			deleteStatement.execute();
			
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "CustomerDAO:delete " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(dbConnection);
			ConnectionFactory.close(deleteStatement);
		}
	}
	public static List<Customer> getAllCustomers(){
		List<Customer> customers = new ArrayList<Customer>();
		Connection dbConnection = ConnectionFactory.getConnection();
		Statement getStatement = null;
		ResultSet rs = null;
		
		try{
			getStatement = dbConnection.createStatement();
			rs = getStatement.executeQuery(getStatementString);
			while(rs.next()){
				int id = rs.getInt("id");
				String nume = rs.getString("nume");
				String email = rs.getString("email");
				String telefon = rs.getString("telefon");
				Date dataCreare = rs.getDate("dataCreare");
				Date ultimaModificare = rs.getDate("ultimaModificare");
				customers.add(new Customer(id,nume,email,telefon,dataCreare,ultimaModificare));
			}	
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "CustomerDAO:getAllCust " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(getStatement);
			ConnectionFactory.close(rs);
			ConnectionFactory.close(dbConnection);
		}
		return customers;
	}
	public static List<Address> getAllAddresses(){
		List<Address> addresses = new ArrayList<Address>();
		Connection dbConnection = ConnectionFactory.getConnection();
		Statement getStatement = null;
		ResultSet rs = null;
		try{
			getStatement = dbConnection.createStatement();
			rs = getStatement.executeQuery(getStatementAddrString);
			while(rs.next()){
				int id = rs.getInt("idCustomer");
				String judet = rs.getString("judet");
				String oras = rs.getString("oras");
				String strada = rs.getString("strada");
				String codPostal = rs.getString("codPostal");
				addresses.add(new Address(id,judet,oras,strada,codPostal));
			}
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "CustomerDAO:getAllAddr " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(getStatement);
			ConnectionFactory.close(rs);
			ConnectionFactory.close(dbConnection);
		}
		return addresses;
		
		
	}
}
