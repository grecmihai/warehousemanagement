package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Customer;
import model.Order;
import model.Product;
import model.Stock;
import model.Struct;

public class OrderDAO {
	protected static final Logger LOGGER = Logger.getLogger(OrderDAO.class.getName());
	private static final String insertStatementString = "insert into orders (customerID,productID,cantitate) values (?,?,?)";
	private static final String findStatementString = "select * from orders where id = ?";
	
	public static Order findById(int orderId){
		Order toReturn = null;
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		
		try{
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setInt(1, orderId);
			rs = findStatement.executeQuery();
			rs.next();
			
			int id = rs.getInt("id");
			int customerId = rs.getInt("customerId");
			Date date = rs.getDate("data");
			int productId = rs.getInt("productId");
			int cantitate = rs.getInt("cantitate");
			
			toReturn = new Order(id,customerId,productId,cantitate,date);
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING,"CustomerDAO:findById " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	public static Struct insert(Order order){
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement insertStatement = null;
		boolean canInsert = true;
		int insertedId = -1;
		
		
		try{
			Customer customer = CustomerDAO.findById(order.getCustomerId());
			Product product = ProductDAO.findById(order.getProductId());
			Stock stock = ProductDAO.findStockById(product.getId());
			if (order.getCantitate() > stock.getCantitate())
				canInsert = false;
			else {
				insertStatement = dbConnection.prepareStatement(insertStatementString,Statement.RETURN_GENERATED_KEYS);
				insertStatement.setInt(1, customer.getId());
				insertStatement.setInt(2, product.getId());
				insertStatement.setInt(3, order.getCantitate());
				insertStatement.executeUpdate();
				ProductDAO.decreaseQuantity(product.getNume(), order.getCantitate());
				ResultSet rs = insertStatement.getGeneratedKeys();
				if (rs.next())
				insertedId = rs.getInt(1);
			}
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING,"OrderDAO:insert " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(dbConnection);
			ConnectionFactory.close(insertStatement);
		}
		return new Struct(canInsert,insertedId);
		
	}
}
