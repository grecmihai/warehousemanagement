package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


import connection.ConnectionFactory;
import model.Product;
import model.Stock;

public class ProductDAO {
	protected static final Logger LOGGER = Logger.getLogger(CustomerDAO.class.getName());
	private static final String findStatementString = "select * from product where id = ?";
	private static final String findStockNameStatementString = "select * from stock join product on id = idProduct where nume = ?";
	private static final String findStockStatementString = "select * from stock where idProduct = ?";
	private static final String findStatementNameString = "select * from product where nume = ?";
	private static final String insertStatementString = "insert into product (nume,descriere,pret) values(?,?,?)";
	private static final String insertStatementStockString = "insert into stock values (?,?)";
	private static final String updateStatementString = "update product set nume = ?, descriere = ?, pret = ? where id = ?";
	private static final String updateStatementStockString = "update stock set cantitate = cantitate + ? where idProduct = ?";
	private static final String decreaseStatementStockString = "update stock set cantitate = cantitate - ? where idProduct = ?";
	private static final String deleteStatementString = "delete from product where nume = ?";
	private static final String getStatementString = "select * from product";
	private static final String getStatementStockString = "select * from stock";
	public static Product findById(int productId){
		Product toReturn = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try{
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setInt(1, productId);
			rs = findStatement.executeQuery();
			rs.next();
			
			String nume = rs.getString("nume");
			String descriere = rs.getString("descriere");
			double pret = rs.getDouble("pret");
			toReturn = new Product(productId,nume,descriere,pret);
		}
		catch(SQLException e){
			LOGGER.log(Level.WARNING,"ProductDAO:findById " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	public static Stock findStockById(int productId){
		Stock toReturn = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try{
			findStatement = dbConnection.prepareStatement(findStockStatementString);
			findStatement.setInt(1, productId);
			rs = findStatement.executeQuery();
			rs.next();
			
			int cantitate = rs.getInt("cantitate");
			toReturn = new Stock(productId,cantitate);
		}
		catch(SQLException e){
			LOGGER.log(Level.WARNING,"ProductDAO:findById " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	public static Product findByName(String name){
		Product toReturn = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try{
			findStatement = dbConnection.prepareStatement(findStatementNameString);
			findStatement.setString(1, name);
			rs = findStatement.executeQuery();
			rs.next();
			
			int id = rs.getInt("id");
			String descriere = rs.getString("descriere");
			double pret = rs.getDouble("pret");
			toReturn = new Product(id,name,descriere,pret);
		}
		catch(SQLException e){
			LOGGER.log(Level.WARNING,"ProductDAO:findById " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	public static Stock findStockByName(String name){
		Stock toReturn = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try{
			findStatement = dbConnection.prepareStatement(findStockNameStatementString);
			findStatement.setString(1, name);
			rs = findStatement.executeQuery();
			rs.next();
			
			int id = rs.getInt("idProduct");
			int cantitate = rs.getInt("cantitate");
			toReturn = new Stock(id,cantitate);
		}
		catch(SQLException e){
			LOGGER.log(Level.WARNING,"ProductDAO:findById " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	public static int insert(Product product, Stock stock){
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement insertStatement= null;
		PreparedStatement insertStatementStock = null;
		int insertedId = -1;
		
		try{
			insertStatement = dbConnection.prepareStatement(insertStatementString,Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, product.getNume());
			insertStatement.setString(2, product.getDescriere());
			insertStatement.setDouble(3, product.getPret());
			insertStatement.executeUpdate();
			
			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next())
				insertedId = rs.getInt(1);
			
			insertStatementStock = dbConnection.prepareStatement(insertStatementStockString);
			insertStatementStock.setInt(1, insertedId);
			insertStatementStock.setInt(2, stock.getCantitate());
			insertStatementStock.executeUpdate();
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO:insert " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(dbConnection);
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(insertStatementStock);
		}
		return insertedId;
	}
	public static void update(Product product,String prod){
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement= null;
		int id = -1;
		
		try{
			Product helper = findByName(prod);
			id = helper.getId();
			
			if (id != -1){
				updateStatement = dbConnection.prepareStatement(updateStatementString);
				updateStatement.setString(1, product.getNume());
				updateStatement.setString(2, product.getDescriere());
				updateStatement.setDouble(3, product.getPret());
				updateStatement.setInt(4, id);
				updateStatement.executeUpdate();
			}
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO:update " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(dbConnection);
			ConnectionFactory.close(updateStatement);
		}
	}
	public static void increaseQuantity(String product,int quantity){
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement= null;
		int id = -1;
		try{
			Product helper = findByName(product);
			id = helper.getId();
			
			if (id != -1){
				updateStatement = dbConnection.prepareStatement(updateStatementStockString);
				updateStatement.setInt(1, quantity);
				updateStatement.setInt(2, id);
				updateStatement.executeUpdate();
				
			}
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO:increaseQuantity " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(dbConnection);
			ConnectionFactory.close(updateStatement);
		}
	}
	public static void decreaseQuantity(String product, int quantity){
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement= null;
		int id = -1;
		try{
			Product helper = findByName(product);
			id = helper.getId();
			
			if (id != -1){
				updateStatement = dbConnection.prepareStatement(decreaseStatementStockString);
				updateStatement.setInt(1, quantity);
				updateStatement.setInt(2, id);
				updateStatement.executeUpdate();
				
			}
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO:increaseQuantity " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(dbConnection);
			ConnectionFactory.close(updateStatement);
		}
	}
	public static void delete(String nume){
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString);
			deleteStatement.setString(1, nume);
			deleteStatement.execute();
			
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO:delete " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(dbConnection);
			ConnectionFactory.close(deleteStatement);
		}
	}
	public static List<Product> getAllProducts(){
		List<Product> products = new ArrayList<Product>();
		Connection dbConnection = ConnectionFactory.getConnection();
		Statement getStatement = null;
		ResultSet rs = null;
		
		try{
			getStatement = dbConnection.createStatement();
			rs = getStatement.executeQuery(getStatementString);
			while (rs.next()){
				int id = rs.getInt("id");
				String nume = rs.getString("nume");
				String descriere = rs.getString("descriere");
				double pret = rs.getDouble("pret");
				products.add(new Product(id,nume,descriere,pret));
			}
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO:getAllProds " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(getStatement);
			ConnectionFactory.close(rs);
			ConnectionFactory.close(dbConnection);
		}
		return products;
	}
	public static List<Stock> getAllStocks(){
		List<Stock> stocks = new ArrayList<Stock>();
		Connection dbConnection = ConnectionFactory.getConnection();
		Statement getStatement = null;
		ResultSet rs = null;
		try{
			getStatement = dbConnection.createStatement();
			rs = getStatement.executeQuery(getStatementStockString);
			while (rs.next()){
				int id = rs.getInt("idProduct");
				int cantitate = rs.getInt("cantitate");
				stocks.add(new Stock(id,cantitate));
			}
		}
		catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO:getAllStocks " + e.getMessage());
		}
		finally{
			ConnectionFactory.close(getStatement);
			ConnectionFactory.close(rs);
			ConnectionFactory.close(dbConnection);
		}
		return stocks;
	}
}
